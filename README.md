## Description
This mod adds vehicle years to all base-game vehicles (tractors, cars, trailers, seeders, etc.)
in Farming Simulator 22.

This project also contains a few scripts to help collect and structure the data in a meaningful way.

## Adding years to your own mod
This is a short guide on how to add years to your own mods.

Simply add a `<year>` entry in your mod's XML file under `<storeData>`.
Your mod's XML should contain something like

```xml
<vehicle>
    <storeData>
        <year>1999</year>
    </storeData>
</vehicle>
```

After this, the Vehicle Years mod should automatically read your mods provided year, instead of
reading the year provided by this mod.
