#!/usr/bin/env python

# This file is part of FS22_Vehicle_Years.
#
# FS22_Vehicle_Years is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# FS22_Vehicle_Years is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# FS22_Vehicle_Years. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from typing import Final
from multiprocessing import Pool

import argparse
import xml.etree.ElementTree as ET
import urllib.request
import re
import zipfile
import shutil

YEAR_TBD: Final[str] = "TBD"

def is_valid_category(category: str) -> bool:
    valid_categories = [
        "animaltransport",
        "animals",
        "animalsvehicles",
        "augerwagons",
        "baleloaders",
        "balewrappers",
        "balers",
        "beetharvesting",
        "beetvehicles",
        "belts",
        "cars",
        "cornheaders",
        "cottonharvesting",
        "cottonvehicles",
        "cultivators",
        "cuttertrailers",
        "cutters",
        "discharrows",
        "dollys",
        "fertilizerspreaders",
        "forageharvestercutters",
        "forageharvesters",
        "forklifts",
        "frontloadertools",
        "frontloadervehicles",
        "frontloaders",
        "grapetools",
        "grapevehicles",
        "grasslandcare",
        "harvesters",
        "leveler",
        "loaderwagons",
        "lowloaders",
        "manurespreaders",
        "misc",
        "miscvehicles",
        "mowervehicles",
        "mowers",
        "mulchers",
        "olivevehicles",
        "planters",
        "plows",
        "potatoharvesting",
        "potatovehicles",
        "powerharrows",
        "rollers",
        "seeders",
        "silocompaction",
        "skidsteertools",
        "skidsteervehicles",
        "slurrytanks",
        "slurryvehicles",
        "spaders",
        "sprayervehicles",
        "sprayers",
        "stonepickers",
        "subsoilers",
        "sugarcaneharvesting",
        "sugarcanevehicles",
        "tedders",
        "teleloadertools",
        "teleloadervehicles",
        "tractorsl",
        "tractorsm",
        "tractorss",
        "trailers",
        "trucks",
        "vegetabletools",
        "vegetablevehicles",
        "weeders",
        "weights",
        "wheelloadertools",
        "wheelloadervehicles",
        "windrowers",
        "winterequipment",
        "wood",
        "woodharvesting",
    ]

    valid = False

    # Some vehicles have multiple categories.
    # If just any of them are valid, we consider the vehicle valid.
    for cat in category.split():
        if cat.lower() in valid_categories:
            valid = True

    return valid


def convert_category(category: str) -> str:
    """
    Takes a category and returns the proper capitalization of it

    This is required since some mods write e.g. PLANTERS instead of planters,
    or MISC instead of misc

    Keyword arguments:
    category (str) -- The category to convert

    """

    categories_lower = {
        "animaltransport": "animalTransport",
        "animals": "animals",
        "animalsvehicles": "animalsVehicles",
        "augerwagons": "augerWagons",
        "baleloaders": "baleLoaders",
        "balewrappers": "baleWrappers",
        "balers": "balers",
        "beetharvesting": "beetHarvesting",
        "beetvehicles": "beetVehicles",
        "belts": "belts",
        "cars": "cars",
        "cornheaders": "cornHeaders",
        "cottonharvesting": "cottonHarvesting",
        "cottonvehicles": "cottonVehicles",
        "cultivators": "cultivators",
        "cuttertrailers": "cutterTrailers",
        "cutters": "cutters",
        "discharrows": "discHarrows",
        "dollys": "dollys",
        "fertilizerspreaders": "fertilizerSpreaders",
        "forageharvestercutters": "forageHarvesterCutters",
        "forageharvesters": "forageHarvesters",
        "forklifts": "forklifts",
        "frontloadertools": "frontLoaderTools",
        "frontloadervehicles": "frontLoaderVehicles",
        "frontloaders": "frontLoaders",
        "grapetools": "grapeTools",
        "grapevehicles": "grapeVehicles",
        "grasslandcare": "grasslandCare",
        "harvesters": "harvesters",
        "leveler": "leveler",
        "loaderwagons": "loaderWagons",
        "lowloaders": "lowloaders",
        "manurespreaders": "manureSpreaders",
        "misc": "misc",
        "miscvehicles": "miscVehicles",
        "mowervehicles": "mowerVehicles",
        "mowers": "mowers",
        "mulchers": "mulchers",
        "olivevehicles": "oliveVehicles",
        "planters": "planters",
        "plows": "plows",
        "potatoharvesting": "potatoHarvesting",
        "potatovehicles": "potatoVehicles",
        "powerharrows": "powerHarrows",
        "rollers": "rollers",
        "seeders": "seeders",
        "silocompaction": "silocompaction",
        "skidsteertools": "skidSteerTools",
        "skidsteervehicles": "skidSteerVehicles",
        "slurrytanks": "slurryTanks",
        "slurryvehicles": "slurryVehicles",
        "spaders": "spaders",
        "sprayervehicles": "sprayerVehicles",
        "sprayers": "sprayers",
        "stonepickers": "stonePickers",
        "subsoilers": "subsoilers",
        "sugarcaneharvesting": "sugarCaneHarvesting",
        "sugarcanevehicles": "sugarcaneVehicles",
        "tedders": "tedders",
        "teleloadertools": "teleLoaderTools",
        "teleloadervehicles": "teleLoaderVehicles",
        "tractorsl": "tractorsL",
        "tractorsm": "tractorsM",
        "tractorss": "tractorsS",
        "trailers": "trailers",
        "trucks": "trucks",
        "vegetabletools": "vegetableTools",
        "vegetablevehicles": "vegetableVehicles",
        "weeders": "weeders",
        "weights": "weights",
        "wheelloadertools": "wheelLoaderTools",
        "wheelloadervehicles": "wheelLoaderVehicles",
        "windrowers": "windrowers",
        "winterequipment": "winterEquipment",
        "wood": "wood",
        "woodharvesting": "woodHarvesting",
    }

    return categories_lower[category.lower()]


class ModZipFile:
    """
    Class to hold information about a mod zip file

    Keyword arguments:
    mod_name (str) -- The mod name
    mod_id (int) -- The mod ID
    rating (float) -- The mod rating
    rating_cnt (int) -- The mod rating count
    is_vehicle_mod (bool) -- Whether or not the mod is a vehicle mod, or e.g. a Map mod

    """

    def __init__(
        self,
        name: str,
        mod_id: int,
        rating: float,
        rating_cnt: int,
        is_vehicle_mod: bool,
    ):
        self.name = name
        self.mod_id = mod_id
        self.rating = rating
        self.rating_str = str(self.rating).replace(".", "_")
        self.rating_cnt = rating_cnt
        self.is_vehicle_mod = is_vehicle_mod
        self.is_vehicle_mod_str = str(int(self.is_vehicle_mod))
        self.file_name = (f"{self.mod_name}_{self.mod_id}_{self.rating_str}_{self.rating_cnt}_"
                          f"{self.is_vehicle_mod_str}.zip")

    @classmethod
    def from_file(cls, file_name: str):
        """
        Parses filename and generates a ModZipFile from the content of it

        Keyword arguments:
        file_name (str) -- The file anme to generate ModZipFile from, e.g.
                           FS22_FrontierMap_272092_4_1_2455_0.zip

        """
        result = re.search(r"/(?P<mod_name>\w+)_"
                           r"(?P<mod_id>\d{6,})_"
                           r"(?P<rating_str>\d_\d)_"
                           r"(?P<rating_cnt>\d+)_"
                           r"(?P<is_vehicle_mod_str>\d)", file_name)
        if result is not None:
            rating = result["rating_str"].replace("_", ".")

            return cls(result["mod_name"], int(result["mod_id"]), float(rating),
                       int(result["rating_cnt"]), bool(int(result["is_vehicle_mod_str"])))

        return None

    def __str__(self):
        return file_name

class ModInfo:
    """
    Class to hold information about a mod

    Keyword arguments:
    id (int) -- The mod ID
    name (str) -- The mod name
    rating (float) -- The mod rating
    rating_cnt (int) -- The mod rating count
    is_vehicle_mod (bool) -- Whether or not the mod is a vehicle mod, or e.g. a Map mod

    """

    def __init__(
        self,
        id: int,
        name: str,
        rating: float,
        rating_cnt: int,
        is_vehicle_mod: bool,
    ):
        self.id = id
        self.name = name
        self.rating = rating
        self.rating_cnt = rating_cnt
        self.is_vehicle_mod = is_vehicle_mod
        self.url = (f"https://www.farming-simulator.com/mod.php?lang=en&country=us&"
                    f"mod_id={id}&title=fs2022")

    @classmethod
    def from_mod_zip_file(cls, mod_zip_file: ModZipFile):
        """
        Parses ModZipFile object and generates a ModInfo from the content of it

        Keyword arguments:
        mod_zip_file (ModZipFile) -- The ModZipFile to generate ModInfo from

        """
        return cls(mod_zip_file.mod_id, mod_zip_file.mod_name, mod_zip_file.rating,
                   mod_zip_file.rating_cnt, mod_zip_file.is_vehicle_mod)

    def __str__(self):
        return (f"ModInfo({self.id}, {self.name}, {self.rating}, {self.rating_cnt}, "
                f"{self.is_vehicle_mod})")

    def __eq__(self, other):
        return (
            self.id == other.id
            and self.name == other.name
        )

class Vehicle:
    """
    Class to hold Vehicle data

    Keyword arguments:
    category (str)         -- The category of the vehicle, e.g. "planters" or "tractorsL"
    brand (str)            -- The brand of the vehicle, e.g. "FENDT" or "NEWHOLLAND"
    name (str)             -- The name of the vehicle, e.g. "MLT 840-145 PS+" or "8R Series"
    raw_xml_filename (str) -- The raw XML filename used by the game.
    year (str)             -- The year of the vehicle as a string, e.g. "1992" or "2001".
                              Defaults to YEAR_TBD if not set.

    """

    def __init__(
        self,
        category: str,
        brand: str,
        name: str,
        raw_xml_filename: str,
        year: str = YEAR_TBD,
        mod_info: ModInfo = None,
    ):
        self.category = category
        self.brand = brand
        self.name = name
        self.raw_xml_filename = raw_xml_filename
        self.year = year
        self.mod_info = mod_info

    def __str__(self):
        return (f"Vehicle({self.category}, {self.brand}, {self.name}, {self.raw_xml_filename}, "
                f"{self.year}, {self.mod_info})")

    def __lt__(self, other):
        if self.category < other.category:
            return True
        elif self.category == other.category:
            if self.brand < other.brand:
                return True
            elif self.brand == other.brand:
                if self.name < other.name:
                    return True
                elif self.name == other.name:
                    return self.raw_xml_filename < other.raw_xml_filename

        return False

    def __eq__(self, other):
        return (
            self.category == other.category
            and self.brand == other.brand
            and self.raw_xml_filename == other.raw_xml_filename
            and self.mod_info == other.mod_info
        )


def parse_xmlfile(input_dir: Path, path: Path, is_mod: bool = False, mod_dir: Path = None) -> Vehicle:
    """
    Parses an XML file containing a single vehicle and returns it as a
    Vehicle object

    Keyword arguments:
    input_dir (Path) -- The root directory of the vehicle
    path (Path)      -- The path of the file to parse
    is_mod (bool)    -- Whether or not this is a mod
    mod_dir (Path)   -- Root directory of the mod

    """
    try:
        root = ET.parse(str(path)).getroot()
    except:
        print(f"Failed to parse {str(path)}")
        return None

    if root.tag == "vehicle":
        store_data = root.find("storeData")
        if store_data is not None:
            show_in_store = store_data.find("showInStore")
            year_str = YEAR_TBD

            # Don"t consider hidden vehicles
            hidden = show_in_store is not None and show_in_store.text == "false"
            if hidden:
                return None

            name = store_data.find("name")
            brand = store_data.find("brand")
            category = store_data.find("category")
            mod_info = None
            raw_xml_filename = ""

            if (
                name is None
                or name.text is None
                or brand is None
                or brand.text is None
                or category is None
                or category.text is None
            ):
                return None

            name_str = name.text.strip()
            brand_str = brand.text.strip().upper()  # brands should always be upper case
            category_str = category.text.strip()
            if not is_valid_category(category_str):
                print((f"Invalid category from {str(mod_dir)} for"
                       f"{brand_str}:{name_str}: {category_str}"))
                return None

            category_str = convert_category(category_str.split()[0])

            if is_mod:
                # Some mods have the name under a subelement for each language
                en_name = name.find("en")
                if en_name is not None:
                    name_str = en_name.text.strip()

                # If the mod declares a year itself, we use that
                mod_year = store_data.find("year")
                if mod_year is not None:
                    year_str = mod_year.text.strip()
                    print(f"Mod has year: {year_str}")

                mod_file_name = ModZipFile.from_file(str(mod_dir))
                if mod_file_name is None:
                    print(f"Invalid directory name: {str(mod_dir)}")
                    return None

                mod_info = ModInfo.from_mod_zip_file(mod_file_name)

                # The rawXMLFilename in the game for mods are always the path of the XML file
                # relative to the customEnvironment (which is the mod directory name, e.g.
                # FS22_Vehicle_Years)
                # raw_xml_filename is based on the file path minus the directory it is found to
                # get the path relative to the mod
                raw_xml_filename = path.as_posix().replace(str(mod_dir) + "/", "")
            else:
                # The rawXMLFilename in the game for non-mods are always
                # $data/vehicles/{brand}/{name}/{xmlName}
                # so modify the XML filepath here to match that
                raw_xml_filename = path.as_posix().replace(input_dir.as_posix(), "$data/vehicles")

            return Vehicle(category_str, brand_str, name_str, raw_xml_filename, year_str,
                           mod_info=mod_info)

    return None


def parse_mod_zip_file(zipfile: Path) -> list[Vehicle]:
    """
    Parses a zipfile of a mod, extracting it and parsing all the .xml files within it, and returns
    the list of vehicles within the mod.

    Keyword arguments:
    zipfile (Path) -- The path of the zip'ed mod
    """
    vehicles = []

    # Extract the mod into a new directory with the zip file name
    print(f"Handling {str(zipfile)}")
    extract_folder_path = zipfile.parent.joinpath(zipfile.stem)
    extract_mod_zip(zipfile, extract_folder_path)

    for xmlfile in list(extract_folder_path.glob("**/*.xml")):
        vehicle = parse_xmlfile(zipfile.parent, xmlfile, True, extract_folder_path)
        if vehicle is not None:
            vehicles.append(vehicle)

    shutil.rmtree(extract_folder_path)

    return vehicles


def parse_all_vehicles(vehicles_dir: Path, is_mods: bool = False) -> list[Vehicle]:
    """
    Parses an directory containing XML files for vehicle and returns it as a
    list of Vehicle object

    Keyword arguments:
    vehicles_dir (Path) -- The path of the directory to parse

    """
    vehicles = []

    if is_mods:
        zip_file_list = list(vehicles_dir.glob("*.zip"))

        with Pool() as pool:
            result = pool.map(parse_mod_zip_file, zip_file_list)
            # Since the mod parser returns a list, we must flatten the list of lists
            vehicles = [item for sublist in result for item in sublist]
    else:
        for xmlfile in list(vehicles_dir.glob("**/*.xml")):
            vehicle = parse_xmlfile(vehicles_dir, xmlfile, is_mods)
            if vehicle is not None:
                vehicles.append(vehicle)

    return vehicles


def parse_existing_data(path: Path) -> list[Vehicle]:
    """
    Parses an file containing an XML file containing existing data previously
    output by this program

    Keyword arguments:
    path (Path) -- The path of the file to parse

    """
    existing_data = []

    if path.exists():
        print(f"Parsing existing data from {str(path.absolute())}")
        existing_xml = ET.parse(str(path))

        if existing_xml is not None:
            existing_root = existing_xml.getroot()

            if existing_root.tag == "vehicles":
                for cat in existing_root:
                    for brand in cat:
                        for data in brand:
                            name = data.find("name")
                            year = data.find("year")
                            mod_id = data.find("mod_id")
                            raw_xml_filename = data.find("rawXMLFilename")

                            if mod_id is not None:
                                mod_name = data.find("mod_name")
                                rating = data.find("rating")
                                rating_cnt = data.find("rating_cnt")
                                is_vehicle_mod = data.find("is_vehicle_mod")

                                vehicle = Vehicle(
                                    cat.tag,
                                    brand.tag,
                                    name.text,
                                    raw_xml_filename.text,
                                    year=year.text,
                                    mod_info=ModInfo(int(mod_id.text),
                                                     mod_name.text,
                                                     float(rating.text),
                                                     int(rating_cnt.text),
                                                     is_vehicle_mod.text == str(True))
                                )
                            else:
                                vehicle = Vehicle(cat.tag, brand.tag, name.text,
                                                  raw_xml_filename.text, year=year.text)

                            existing_data.append(vehicle)
        print(f"Found {len(existing_data)} existing vehicles")

    return existing_data


def find_existing_vehicle(existing_data: list[Vehicle], vehicle: Vehicle) -> Vehicle:
    """
    Find existing Vehicle in a list of Vehicles by value

    Keyword arguments:
    existing_data (list[Vehicle]) -- The list of Vehicles
    vehicle (Vehicle)             -- The Vehicle to find

    """
    for v in existing_data:
        if v == vehicle:
            return v

    return None


def store_results(vehicles: list[Vehicle], data_path: Path = None) -> bool:
    """
    Stores the results of parsing FS22 game data and any existing data if
    the data_path is set, otherwise just prints the resulting data

    Keyword arguments:
    vehicles (list[Vehicle]) -- The list of Vehicle objects parsed from FS22 game data
    data_path (Path)         -- The path of the output file (if supplied). This will be used to
                                avoid overwriting any existing data while still adding new data

    """
    root_elem = ET.Element("vehicles")
    existing_data = None
    category = ""
    brand = ""

    if data_path is not None:
        existing_data = parse_existing_data(data_path)

        for vehicle in existing_data:
            if vehicle not in vehicles:
                vehicles.append(vehicle)

    vehicles.sort()  # sort by category first, then brand, then name

    # Generate XML and if the vehicle exists in the existing data, then use the
    # year from existing data to avoid overriding it with YEAR_TBD
    for vehicle in vehicles:
        if category != vehicle.category:
            category = vehicle.category
            cat_elem = ET.SubElement(root_elem, category)

        if brand != vehicle.brand:
            brand = vehicle.brand
            brand_elem = ET.SubElement(cat_elem, brand)

        vehicle_elem = ET.SubElement(brand_elem, "vehicle")

        name_elem = ET.SubElement(vehicle_elem, "name")
        name_elem.text = vehicle.name

        year_text = vehicle.year
        if existing_data:
            existing_vehicle = find_existing_vehicle(existing_data, vehicle)

            if existing_vehicle is not None and year_text == YEAR_TBD:
                year_text = existing_vehicle.year

        year_elem = ET.SubElement(vehicle_elem, "year")
        year_elem.text = year_text

        xmlfile_elem = ET.SubElement(vehicle_elem, "rawXMLFilename")
        xmlfile_elem.text = vehicle.raw_xml_filename

        if vehicle.mod_info is not None:
            id_elem = ET.SubElement(vehicle_elem, "mod_id")
            id_elem.text = str(vehicle.mod_info.id)

            mod_name_elem = ET.SubElement(vehicle_elem, "mod_name")
            mod_name_elem.text = vehicle.mod_info.name

            url_elem = ET.SubElement(vehicle_elem, "mod_url")
            url_elem.text = vehicle.mod_info.url

            rating_elem = ET.SubElement(vehicle_elem, "rating")
            rating_elem.text = str(vehicle.mod_info.rating)

            rating_cnt_elem = ET.SubElement(vehicle_elem, "rating_cnt")
            rating_cnt_elem.text = str(vehicle.mod_info.rating_cnt)

            is_vehicle_mod_elem = ET.SubElement(vehicle_elem, "is_vehicle_mod")
            is_vehicle_mod_elem.text = str(vehicle.mod_info.is_vehicle_mod)

    print("Total: " + str(len(vehicles)))

    elem_tree = ET.ElementTree(root_elem)
    ET.indent(elem_tree, space="\t")
    if data_path is not None:
        elem_tree.write(str(data_path), encoding="unicode", xml_declaration=True)

        print("Saved file to " + str(data_path.absolute()))
    else:
        for vehicle in vehicles:
            print(str(vehicle))

    return True


def download_mod_zip(zip_file_path: Path, zip_url: str, zip_dir_path: Path):
    zip_file_path_abs = str(zip_file_path.absolute())

    # Ensure that the directory to store the zips in exist
    if not zip_dir_path.is_dir():
        zip_dir_path.mkdir()

    # Only down zip file if it doesn't already exist
    if not zip_file_path.is_file():
        print(f"Downloading {zip_url}")
        #  Add "Referer" to avoid a 403 from server
        req = urllib.request.Request(zip_url)
        req.add_header("Referer", "https://www.farming-simulator.com/")

        with urllib.request.urlopen(req) as zip_file_req:
            with open(zip_file_path_abs, "wb") as out_file:
                out_file.write(zip_file_req.read())
    else:
        print(f"{zip_file_path_abs} already exist")


def extract_mod_zip(zip_file_path: Path, output_path: Path):
    with zipfile.ZipFile(zip_file_path, "r") as zip_file:
        zip_file.extractall(output_path)


def parse_and_download_mod(mod_id: str, data_path: Path):
    # List of categories to ignore
    nonvehicle_cats: Final[list[str]] = [
        "Map",
        "Gameplay",
        "Bigbag Pallets",
        "Bigbags",
        "Pallets",
        "Sheds",
        "Silos",
        "Container",
        "Farmhouses",
        "Factories",
        "Selling Points",
        "Greenhouses",
        "Generators",
        "Animal Pens",
        "Decoration",
        "Prefab",
    ]

    # Compare the mod_id to the list of downloaded files to avoid re-downloading it
    if data_path is not None:
        for downloaded_mod in list(data_path.glob("*.zip")):
            mod_file_name = ModZipFile.from_file(str(downloaded_mod))

            if mod_file_name is not None:
                if mod_id == mod_file_name.mod_id:
                    print(f"Mod with id {mod_id} is already downloaded")
                    return

    mod_url = (
        f"https://www.farming-simulator.com/mod.php?lang=en&country=us&mod_id={mod_id}&title=fs2022"
    )

    print(f"Opening {mod_url}")

    try:
        with urllib.request.urlopen(mod_url) as req:
            html = req.read().decode("utf-8")

            result = re.search(
                r"Category.*>(?P<category>\w+[ \w+]*)<.*>Author<.*"
                r"mod-item__rating-num float-left\">(?P<rating>\d(\.\d)?)&nbsp;"
                r"\((?P<rating_cnt>\d+).*"
                r"(?P<url>https\S+/\d+/(?P<zip_name>\w+).zip).+>DOWNLOAD<",
                html,
                re.DOTALL,
            )

            if result is not None:
                print(result.groupdict())
                category = result["category"]

                is_vehicle_mod = category not in nonvehicle_cats

                if data_path is not None:
                    zip_name = result["zip_name"]
                    url = result["url"]
                    zip_file_name = ModZipFile(zip_name, int(mod_id), float(
                        result["rating"]), int(result["rating_cnt"]), is_vehicle_mod)
                    zip_file_path = data_path.joinpath(zip_file_name.file_name)

                    # Download the mod and store the .zip file in the zips directory
                    download_mod_zip(zip_file_path, url, data_path)
            pass
    except urllib.error.URLError as e:
        print(f"Failed to open URL with reason {e.reason}")


def download_mods(page_count: int, data_path: Path = None):
    page = 0
    mod_ids = []

    while True:
        page_url = (f"https://www.farming-simulator.com/mods.php?lang=en&country=us&title=fs2022&"
                    f"filter=latest&page={page}")

        print(f"Loading page {page}")
        with urllib.request.urlopen(page_url) as req:
            page_compare_str = f'<span class="show-for-sr">You\'re on page</span> {page + 1}'

            html = req.read().decode("utf-8")
            result = re.findall(r"mod-item\_\_img\">.*\n.*\n.+mod_id=(?P<mod_id>\d+)", html)
            if result is not None:
                mod_ids.extend(result)

            if not page_compare_str in html:
                print(f"Stopping at page {page}")
                break

        page = page + 1

        if page_count > 0 and page >= page_count:
            break

    with Pool() as pool:
        pool.starmap(parse_and_download_mod, [(mod_id, data_path) for mod_id in mod_ids])


def generate_vehicle_list_by_category(vehicles_list: list[Vehicle], data_path: Path = None):
    result = ""
    categories = {}

    for vehicle in vehicles_list:
        if vehicle.year != YEAR_TBD:
            if vehicle.category not in categories:
                categories[vehicle.category] = []

            categories[vehicle.category].append(vehicle)

    for category, vehicles in categories.items():
        result += f"{category}:\n"

        vehicles.sort(key=lambda x: x.year)
        for vehicle in vehicles:
            if vehicle.mod_info is not None:
                result += f"\t{vehicle.year}: {vehicle.brand} - {
                    vehicle.name} (Mod: {vehicle.mod_info.name}: {vehicle.mod_info.url})\n"
            else:
                result += f"\t{vehicle.year}: {vehicle.brand} - {vehicle.name}\n"

    if data_path is not None:
        with open(data_path.absolute(), "w") as out_file:
            out_file.write(result)
    else:
        print(result)


def generate_vehicle_list_by_year(vehicles_list: list[Vehicle], data_path: Path = None):
    result = ""

    years = {}

    for vehicle in vehicles_list:
        if vehicle.year != YEAR_TBD:
            if vehicle.year not in years:
                years[vehicle.year] = []

            years[vehicle.year].append(vehicle)

    for year, vehicles in sorted(years.items()):
        result += f"{year}:\n"

        vehicles.sort(key=lambda x: x.category)
        for vehicle in vehicles:
            if vehicle.mod_info is not None:
                result += (f"\t{vehicle.category}: {vehicle.brand} - {vehicle.name} "
                           f"(Mod: {vehicle.mod_info.name}: {vehicle.mod_info.url})\n")
            else:
                result += f"\t{vehicle.category}: {vehicle.brand} - {vehicle.name}\n"

    if data_path is not None:
        with open(data_path.absolute(), "w") as out_file:
            out_file.write(result)
    else:
        print(result)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Arguments for the year generator")

    subparsers = parser.add_subparsers(dest="subparser_name", help="sub-command help")

    # Basegame parsers
    basegame_parser = subparsers.add_parser(
        "basegame",
        help="Arguments for the basegame commands used to extract data from a FS22 installation",
    )
    basegame_parser.add_argument(
        "input_dir",
        type=str,
        help="The directory of the FS22 vehicles directory. Typically something like "
             "<user_specific>/Farming Simulator 22/data/vehicles/",
    )
    basegame_parser.add_argument(
        "--output_dir",
        required=False,
        help="The directory of where the outputting XML file will be placed. "
             "May be omitted to do a dry run",
    )

    # Mods parsers
    mods_parser = subparsers.add_parser(
        "mods",
        help="Arguments for the mods commands used to download and extract data from modhub",
    )

    mod_subparsers = mods_parser.add_subparsers(dest="mod_subparser_name", help="sub-command help")

    # Download Mods parsers
    mod_download_parser = mod_subparsers.add_parser(
        "download",
        help="Arguments for the mods commands used to download and extract data from modhub",
    )
    mod_download_parser.add_argument(
        "--page_count",
        type=int,
        default=0,
        help="The number of pages to process and download from. "
             "The default value of 0 indicates all. ",
    )
    mod_download_parser.add_argument(
        "--output_dir",
        required=False,
        help="The directory of where the outputting XML file will be placed. "
             "May be omitted to do a dry run",
    )

    # Generate XML Mods parsers
    mod_xml_parser = mod_subparsers.add_parser(
        "xml",
        help="Arguments for the mods commands used to generate XML file from downloaded mods",
    )
    mod_xml_parser.add_argument(
        "mod_input_dir", type=str, help="The directory of downloaded mods directory"
    )
    mod_xml_parser.add_argument(
        "--mod_output_dir",
        required=False,
        help="The directory of where the outputting XML file will be placed. "
             "May be omitted to do a dry run",
    )

    # Generate vehicle lists parsers
    analyze_parser = subparsers.add_parser(
        "download_list",
        help="Arguments for the download_list commands used to the text files with all vehicles",
    )
    analyze_parser.add_argument(
        "--list_output_dir",
        required=False,
        help="The directory of where the outputting XML file will be placed. "
             "May be omitted to output to stdout",
    )

    args = parser.parse_args()

    if args.subparser_name and args.subparser_name == "basegame":
        input_path = Path(args.input_dir)
        vehicles = parse_all_vehicles(input_path)

        if args.output_dir:
            output_file_path = Path(args.output_dir + "vehicle_years.xml")
            store_results(vehicles, output_file_path)
        else:
            store_results(vehicles)

    if args.subparser_name == "mods":
        if args.mod_subparser_name == "download":
            print("page_count: " + str(args.page_count))

            if args.output_dir:
                output_file_path = Path(args.output_dir)
                download_mods(args.page_count, output_file_path)
            else:
                download_mods(args.page_count)

        if args.mod_subparser_name == "xml":
            input_path = Path(args.mod_input_dir)
            vehicles = parse_all_vehicles(input_path, True)

            for v in vehicles:
                print(str(v))

            if args.mod_output_dir:
                output_file_path = Path(args.mod_output_dir + "vehicle_years_mods.xml")
                store_results(vehicles, output_file_path)
            else:
                store_results(vehicles)

    if args.subparser_name == "download_list":
        base_game_vehicles = parse_existing_data(Path("data").joinpath("vehicle_years.xml"))
        mod_vehicles = parse_existing_data(Path("data").joinpath("vehicle_years_mods.xml"))

        vehicles = base_game_vehicles + mod_vehicles

        generate_vehicle_list_by_category(vehicles, Path("data").joinpath("category_list.txt"))
        generate_vehicle_list_by_year(vehicles, Path("data").joinpath("year_list.txt"))
